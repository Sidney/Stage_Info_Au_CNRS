#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#include <poll.h>
#include <byteswap.h>
#include <inttypes.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#define MAX_PACKET_SIZE 9000

int main(int argc, char *argv[], char *arge[])
{int rv;
int sock;	// Receive socket
int port;	// Receive port
int ExpectedSize=7824;
int run;
char data[MAX_PACKET_SIZE];

/* Set up socket */
sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
if (sock==-1) {
  printf("error socket()\n");
  return(-1);
}
printf("socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP) successfully initialized\n");

port=4348;
if(argc>1) sscanf(argv[1],"%d",&port);


// -- Bind to the proper port
// see http://stackoverflow.com/questions/3057029/do-i-have-to-bind-an-udp-socket-in-my-client-program-to-receive-data-i-always
struct sockaddr_in local_ip;
local_ip.sin_family =  AF_INET;
local_ip.sin_port = htons(port);
local_ip.sin_addr.s_addr = INADDR_ANY;
rv = bind(sock, (struct sockaddr *)&local_ip, sizeof(local_ip));
// why the line : rv = bind(sock, (struct sockaddr_in *)&local_ip, sizeof(local_ip));
// does complain about ? ...
if (rv==-1) {
  printf("%s> Error bind()\n",argv[0]);
  return(-2);
}
printf("bind(AF_INET, port=%d, INADDR_ANY) successfully initialized\n",port);

struct sockaddr_storage peer_addr;
socklen_t peer_addr_len = sizeof(struct sockaddr_storage);
run=1;
while(run) {

  printf("trying recvfrom() ...\n");
  rv = recvfrom(sock, data, MAX_PACKET_SIZE, 0,(struct sockaddr *)&peer_addr, &peer_addr_len);
  printf("   a packet (%d bytes) was received !\n",rv);
  if (rv==-1) {
    printf("   error recvfrom -1\n");
    continue;
  } else {
    if (rv!=ExpectedSize) {
      printf("   error recv size= %d while ExpectedSize= %d\n",rv,ExpectedSize);
    } else {
      printf("   recvfrom OK\n");
    }
  }
}

return(0);
}
