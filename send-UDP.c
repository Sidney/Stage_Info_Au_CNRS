#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <errno.h>

int main () {
int handle;
unsigned short port = 4348;
char node[64]="127.0.0.1";
char * data = "hovno";
int cpt=0;

handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
if (handle <= 0) {
   printf("failed to create socket\n");
   return 1;
}
printf("socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP) successfully initialized\n");

struct sockaddr_in address;
memset((char *) &address, 0, sizeof(address));
address.sin_family = AF_INET;
address.sin_addr.s_addr = inet_addr(node); // this is address of host which I want to send the socket
address.sin_port = htons(port); // this is the port I will be using

printf("socket handle is %d\n", handle); // prints number greater than 0 so I assume handle is initialized properly
printf(" will be used to send UDP packets to node %s on port %d\n",node,port);

while (1) {
  int sent_bytes = sendto(handle, data, strlen(data), 0, (const struct sockaddr*) &address, sizeof(struct sockaddr_in));

  if (sent_bytes != strlen(data)) {
     printf("failed to send packet\n");
     return 3;
  }
  printf("Just sent packet # %06d (%d bytes)...\n",cpt,sent_bytes);
  cpt++;
  sleep(1);
}

return 0;
}
