#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <errno.h>
#include <pcap/pcap.h>

int main(int argc, char *argv[], char *arge[])
{int i,ipcpt;
char file[128];
char errbuf[PCAP_ERRBUF_SIZE];
struct pcap_pkthdr *packet_header;
const u_char *packet;
int offset=42;
pcap_t *handle;
int plen;
int nethandle,netsize;
unsigned short port = 4348;
char node[64]="127.0.0.1";
int cpt=0;

// -- Allocation of memory for packet
packet=(u_char *)malloc(16*1024);

// -- Get the file name from first argument
if(argc>1) sscanf(argv[1],"%s",file);

// -- Open the .pcap file
if ((handle = pcap_open_offline(file, errbuf)) == NULL) {
  printf("unable to open %s ... \n",file);
} else {
  printf("pcap_open_offline(file=<%s>) OK\n",file);
}

// -- Initialize the socket
nethandle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
if (nethandle <= 0) {
   printf("failed to create socket\n");
   return 1;
}
printf("socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP) successfully initialized\n");
// -- Prepare send data over the network
struct sockaddr_in address;
memset((char *) &address, 0, sizeof(address));
address.sin_family = AF_INET;
address.sin_addr.s_addr = inet_addr(node); // this is address of host which I want to send the socket
address.sin_port = htons(port); // this is the port I will be using
printf("socket handle is %d\n", nethandle); // prints number greater than 0 so I assume handle is initialized properly
printf(" will be used to send UDP packets to node %s on port %d\n",node,port);

// -- Initialize packet counter
ipcpt=0;

while(1) {
 // Get one packet
 //- old way : packet = pcap_next(handle, packet_header);
 printf("trying to get packet #%05d with pcap_next_ex() ...\n",ipcpt);
 if( (pcap_next_ex(handle, &packet_header,&packet)!=1) ) {
   printf("  no more packet available in file %s ... Exit.\n",file);
   break;
 }
 printf("  packet_header->ts= %s",ctime((const time_t *) &packet_header->ts.tv_sec));
 printf("  packet_header->ts= %ld.%06ld\n", packet_header->ts.tv_sec, packet_header->ts.tv_usec);
 plen=packet_header->len; netsize=plen-offset;
 printf("  packet_header->len= %u\n",plen);
 //printf("  packet content =\n");
 //for(i=0;i<50;i++) { printf(" %02x",packet[i]); if(i%25==24)printf("\n"); } printf("\n");
 //for(i=plen-50;i<plen;i++) { printf(" %02x",packet[i]); if(i%25==24)printf("\n"); } printf("\n");
 printf("  DATA packet content (real size %d) =\n",netsize);
 for(i=0;i<49;i++) { printf(" %02x",packet[offset+i]); if(i%25==24)printf("\n"); } printf("\n");
 printf(" NOF_BEAMLETS_PER_BANK =  %02x\n",packet[offset+6]);
 printf(" NOF_BLOCKS            =  %02x\n",packet[offset+7]);
 printf(" TIMESTAMP             = "); for(i=0;i<4;i++) printf(" %02x",packet[offset+8+i]); printf("\n");
 printf(" BLOCK_SEQUENCE_NUMBER = "); for(i=0;i<4;i++) printf(" %02x",packet[offset+12+i]); printf("\n");

 int sent_bytes = sendto(nethandle, packet, netsize, 0, (const struct sockaddr*) &address, sizeof(struct sockaddr_in));
 if (sent_bytes != netsize) {
     printf("failed to send packet\n"); return(-3);
 }
 printf("Just sent packet # %06d (%d bytes)...\n",cpt,sent_bytes);

 printf("\n");
 ipcpt++;
}

printf("\n");
pcap_close(handle);
printf("pcap_close() OK\n");
return(0);
}
