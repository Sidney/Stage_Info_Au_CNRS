/************************************
 *									*
 * 			Programme de :			*
 * 	- Lecture d'un fichier '.asc'		*
 * 	- Détermination du maximum 	*
 * 	- Calcul du temps d'arrivée 	 	*
 * 	- Production d'un fichier '.t2' 		*
 * 									*
 ***********************************
*/

#include<iostream> // Pour les cin, cout.
#include<fstream> // Pour le type ofstream.
#include<sstream> // Pour la méthode sscan().
#include<cstdlib> // Pour la méthode rand().
#include<stdio.h> // Pour la méthode printf().
#include<string.h> // Pour la méthode sprintf().
#include<iomanip> // Pour la méthode setprecision().

using namespace std;	//  Pour éviter d'avoir à taper 'std::' tout le temps.

double alea(double M, double N)
/*!
 * Génère un nombre flottant aléatoire entre M et N.
 */
{
    return M + (rand() / ( RAND_MAX / (N-M) ) ) ;  
}

int taille(const char * nom_fichier)
/*!
 * Retourne le nombre de ligne du fichier.
 */
{
	FILE *pf;			// Définit un pointeur sur le fichier.
	int cpt=0;			// Initialise un compteur à 0.
	char ligne[256];			// Définit une chaîne de caractère vide de 256 caractères.
	if((pf=fopen(nom_fichier,"r")) != NULL)			// Si le fichier s'ouvre.
	{
	fgets(ligne,256,pf);			// Lit jusqu'au premier retour chariot et pointe pf sur le début de ce qui nous intéresse de compter.
		while(!feof(pf))			// Critère d'arrêt : Si le pointeur du fichier est nul.
 		{
 			fgets(ligne,256,pf);			// Avance la boucle d'une ligne et la copie dans la variable : ligne.
 			cpt++;			// Incrémente le compteur.
 		}
 	}
 	else			// Si le fichier ne s'ouvre pas.
	{
		cout<<"Impossible d'ouvrir le fichier."<<endl;			// Alerte l'utilisateur : le fichier ne s'est pas ouvert.
	}
	return cpt;			// Retourne la valeur du compteur.
}

long double ToA(long double mjd, long double tudep, long double period, int indice_max, int i1, long double initphase)
/*!
 * Calcule le temps d'arrivée de l'observation.
 */
{
	long double tps=mjd+((tudep+(indice_max/i1)*period-period*initphase)/86400);
	return tps;
}

long double ToA_V1(long double mjd, long double tudep, long double period, int indice_max, int i1, long double initphase)
/*!
 * Calcule le temps d'arrivée de l'observation.
 */
{
	long double tps, E0, E1, E2, E3, E4, E5, Efinal;
	E0=tudep/86400;
	E1=(indice_max/i1);
	E2=E1*period;
	E3=E2/86400;
	E4=(period*initphase);
	E5=E4/86400;
	Efinal=mjd+E0+E3-E5;
	tps=Efinal;
	return tps;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main()
{
	/*** Lecture d'un fichier '.asc' ***/
	
	const char * nom_fichier="1744/1744-1134.1336.54651.1343.asc";			// Ecrit le nom du fichier dans la variable (de type char) : nom_fichier.

	long double mjd, tudep, period, freq_mi_canal, DM, initphase, tps_integration, freq;			// Créé des variables (de type double) pour récupérer les nombres flottants du scan 1.	
	char nom_PSR;			// Créé une variable nom_PSR (de type char) pour récupérer le nom du pulsar à partir du scan 1.
	int i0, i1, i2, code_obs;			// Créé des variables i0, i1, i2 et code_obs (de type int) pour récupérer les entiers du scan 1.
	char ligne[256];			// Déclare une variable (de type char et de taille 256) pour récupérer la première ligne du scan 1.
	
	long double max=0.0,valeur_colonne1, valeur_colonne2, valeur_colonne3, valeur_colonne4; 			// Créé des variables (de type double) pour calculer le maximum et récupérer les valeurs de chaque colonne du scan 2.
	long double tab_colonne1[taille(nom_fichier)];			// Créé un tableau vide (de type double) pour récupérer chaque valeur_colonne1 du scan 2.
	int position, indice_max, indice=0;			// Créé des variables (de type int) pour récupérer l'entier de la colonne la plus à gauche, l'indice du maximum et l'indice des éléments du tableau tab_colonne1 pour le scan 2.
	char valeurs[256];			// Créé une variable (de type char et de taille 256) pour récupérer ligne par ligne les valeurs du scan 2.
	
	FILE *pf;			// Créé un pointeur de fichier : pf.
	if((pf=fopen(nom_fichier,"r")) != NULL)			// Si le fichier s'ouvre.
	{
		fgets(ligne,256,pf);			// Lit la première ligne et la copie dans la chaîne : ligne.
		// Scan 1 //
		sscanf(ligne,"# %Lf %Lf %Lf %d %Lf %Lf %d %d %d %s %Lf %Lf %Lf",&mjd,&tudep,&period,&i0,&freq_mi_canal,&DM,&i1,&code_obs,&i2,&nom_PSR,&initphase,&tps_integration,&freq);		// Scanne la ligne et range les 																																											éléments du fichier dans les adresses des variables adéquates.
		while(!feof(pf))			// Critère d'arrêt : Si le pointeur du fichier est nul.
 		{
 			// Scan 2 //
 			fgets(valeurs,256,pf);			// Lit la ligne et la copie dans la chaîne : valeurs.
 			sscanf(valeurs,"%d %Lf %Lf %Lf %Lf",&position,&valeur_colonne1,&valeur_colonne2,&valeur_colonne3,&valeur_colonne4);			// Scanne la ligne de valeurs et range les éléments dans les variables adéquates.
 			tab_colonne1[indice]=valeur_colonne1;			// Ajoute au tableau_colonne1 la valeur_colonne1 pour la ligne d'indice : indice.
 			if(max<=valeur_colonne1)
 			{
 				max=valeur_colonne1;			// La variable max prend la valeur de cet élément.
 				indice_max=indice;			// Sauvegarde de la position du max dans la variable : indice_max.
 			}
 			indice++;			// Incrémente indice.
 		}
 	fclose(pf);			// Ferme le fichier.
	}
	else			// Si le fichier ne s'ouvre pas.
	{
		cout<<"Impossible d'ouvrir le fichier."<<endl;			// Alerte l'utilisateur : le fichier ne s'est pas ouvert.
	}
	
	/*** Création du fichier '.t2' ***/
	
	ofstream nouveau_fichier;			// Définit un pointeur : nouveau_fichier.
	char lab;			// Définit une variable lab (de type char) pour identifier l'observatoire d'où proviennent les données.
	nouveau_fichier.open("1744.t2", ios::out);			// Créé un nouveau fichier avec la capacité d'écrire dedans.
	if (nouveau_fichier.bad()) 			// Si le fichier ne s'est pas créé.
	{
		cout<<"Impossible de créer le fichier."<<endl;			// Alerte l'utilisateur : Le fichier ne s'est pas créé.
	}
	else			// Si le fichier est créé, on l'ouvre.
	{
	
		/*** Ecriture du fichier '.t2' ***/	
	
		nouveau_fichier<<"FORMAT 1"<<endl;			// Remplit la première ligne du fichier avec "FORMAT 1".
		if(code_obs==15)			// S'il s'agit bien du code de l'observatoire de Nançay.
		{
			lab='f';			// On enregistre la provenance des données comme étant l'observatoire de Nançay.
		}
		else	// S'il ne s'agit pas de l'observatoire de Nançay.
		{
			lab='?';			// On enregistre la provenance des données inconnue.
		}
		nouveau_fichier<<nom_fichier<<" "<<setprecision(7)<<freq_mi_canal<<" ";			// Remplit le fichier du nom du fichier '.asc' puis de la fréquence de mi-canal.
		nouveau_fichier<<setprecision(15)<<ToA_V1(mjd, tudep, period, indice_max, i1, initphase)<<" ";			// Remplit le fichier avec le résultat du calcul du temps d'arrivée. 
		nouveau_fichier<<setprecision(5)<<alea(0.001,99.999)<<" "<<lab<<" "<<"-i BON"<<endl;			// Remplit le fichier de l'incertitude, du nom de l'observatoire et de "-i BON".
		nouveau_fichier.close();			// Ferme le fichier.
	}
	/*** Tests ***/
	
	printf("Temps d'arrivée 1 : %.15Lf \n",ToA_V1(mjd,tudep,period,indice_max,i1,initphase));
	printf("Temps d'arrivée 1 : %.15Lf \n",ToA(mjd,tudep,period,indice_max,i1,initphase));
	printf("Temps d'arrivée 2 : %.15Lf \n",(ToA_V1(mjd,tudep,period,indice_max,i1,initphase)+1*10e-6));
	printf("Add vérif :  %.15Lf \n",ToA_V1(mjd,tudep,period,indice_max,i1,initphase)+ToA_V1(mjd,tudep,period,indice_max,i1,initphase));
	printf("Add vérif :  %.15Lf \n",ToA(mjd,tudep,period,indice_max,i1,initphase)+ToA(mjd,tudep,period,indice_max,i1,initphase));
	printf("Add vérif :  %Lf \n",ToA_V1(mjd,tudep,period,indice_max,i1,initphase)+ToA_V1(mjd,tudep,period,indice_max,i1,initphase));
	printf("Add vérif :  %Lf \n",ToA(mjd,tudep,period,indice_max,i1,initphase)+ToA(mjd,tudep,period,indice_max,i1,initphase));

	long double gigalong=4887.165415454101546562543517543154294;
	cout<<"Le long double : "<<gigalong<<endl;
	cout<<"Addition double : [9774.33083] == "<<setprecision(8)<<(gigalong+gigalong)<<endl;
	
	system("more 1744.t2");			// Affiche le fichier '1744.t2'.
	
	/*** Fin du programme ***/
	
	return 0;			// Cloture le main().
}
