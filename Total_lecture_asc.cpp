/*
 ***************************************
 *																		*
 * 			Programme de :							*
 * 	- Lecture des fichiers '.asc'			*
 * 	- Détermination des maximums 	*
 * 	- Calcul des temps d'arrivée 	 	*
 * 	- Production d'un fichier '.t2' 		*
 * 						            							*
 ***************************************
*/

#include<iostream> // Pour les cin, cout.
#include<fstream> // Pour le type ofstream.
#include<sstream> // Pour la méthode sscan().
#include<cstdlib> // Pour la méthode rand().
#include<stdio.h> // Pour la méthode printf().
#include<string.h> // Pour la méthode strlen().
#include<iomanip> // Pour la méthode setprecision().
#include<cstring> // Pour le type string.

using namespace std;	//  Pour éviter d'avoir à taper 'std::' tout le temps.

double alea(double M, double N)
/*!
 * Génère un nombre flottant aléatoire entre M et N.
 */
{
    return M + (rand() / ( RAND_MAX / (N-M) ) ) ;  
}

int taille(const char * nom_fichier)
/*!
 * Retourne le nombre de ligne du fichier.
 */
{
	FILE *pf;			// Définit un pointeur sur le fichier.
	int cpt=0;			// Initialise un compteur à 0.
	char ligne[256];			// Définit une chaîne de caractère vide de 256 caractères.
	if((pf=fopen(nom_fichier,"r")) != NULL)			// Si le fichier s'ouvre.
	{
	fgets(ligne,256,pf);			// Lit jusqu'au premier retour chariot et pointe pf sur le début de ce qui nous intéresse de compter.
		while(!feof(pf))			// Critère d'arrêt : Si le pointeur du fichier est nul.
 		{
 			fgets(ligne,256,pf);			// Avance la boucle d'une ligne et la copie dans la variable : ligne.
 			cpt++;			// Incrémente le compteur.
 		}
 	}
 	else			// Si le fichier ne s'ouvre pas.
	{
		cout<<"Impossible d'ouvrir le fichier."<<endl;			// Alerte l'utilisateur : le fichier ne s'est pas ouvert.
	}
	return cpt;			// Retourne la valeur du compteur.
}

long double ToA(long double mjd, long double tudep, long double period, int indice_max, int i1, long double initphase)
/*!
 * Calcule le temps d'arrivée de l'observation.
 */
{
	long double tps=mjd+((tudep+(indice_max/i1)*period-period*initphase)/86400);
	return tps;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[], char *arge[])
/*!
 * 'argc' contient le nombre total de mots sur la ligne de commande (incluant le programme lui-même... c'est pour cela   que l'on commence a 1 et non pas 0 dans la boucle plus bas),
 * 'argv[]' est un tableau de chaine de caractères qui contient tous les mots se trouvant sur la ligne de commande,
 * il suffit alors de lancer ton programme avec la commande '	*.asc' et le shell va interpréter "*.asc" et le remplacer par la liste de tous les fichiers de cette forme qu'il trouve dans le dossier courant, 
 * ils seront alors traités les uns après les autres...
 */
{
	char nom_fichier[256];			// Initialise le pointeur (de type char) : fichier. 	
	bool premier_tour=true;		// Initialise une variable (de type bool) pour réaliser certaines actions qu'une seule fois dans la boucle.
	ofstream nouveau_fichier;			// Définit un pointeur : nouveau_fichier.
	
	for(int i=1;i<argc;i++) 		// Pour i allant de 1 à la dernière réponse de la commande tapée.
	{
  		strcpy(nom_fichier,argv[i]);		// Copie le 2e, 3e, 4e... mot du terminal dans fichier.
  		
 		long double mjd, tudep, period, freq_mi_canal, DM, initphase, tps_integration, freq;			// Créé des variables (de type double) pour récupérer les nombres flottants du scan 1.	
		char nom_PSR[256];			// Créé une variable nom_PSR (de type char) pour récupérer le nom du pulsar à partir du scan 1.
		int i0, i1, i2, code_obs;			// Créé des variables i0, i1, i2 et code_obs (de type int) pour récupérer les entiers du scan 1.
		char ligne[256];			// Déclare une variable (de type char et de taille 256) pour récupérer la première ligne du scan 1.
		long double max=0.0, valeur_colonne1, valeur_colonne2, valeur_colonne3, valeur_colonne4; 			// Créé des variables (de type double) pour calculer le maximum et récupérer les valeurs de chaque colonne du scan 2.
		int position, indice_max, indice=0;			// Créé des variables (de type int) pour récupérer l'entier de la colonne la plus à gauche, l'indice du maximum et l'indice des éléments du tableau tab_colonne1 pour le scan 2.
		char valeurs[256];			// Créé une variable (de type char et de taille 256) pour récupérer ligne par ligne les valeurs du scan 2.
		long double tab_colonne1[taille(nom_fichier)];			// Créé un tableau vide (de type double) pour récupérer chaque valeur_colonne1 du scan 2.
		
  		FILE *pf;			// Créé un pointeur de fichier : pf.
		if((pf=fopen(nom_fichier,"r")) != NULL)			// Si le fichier s'ouvre.
		{
			fgets(ligne,256,pf);			// Lit la première ligne et la copie dans la chaîne : ligne.
  			// Scan 1 //
  			sscanf(ligne,"# %Lf %Lf %Lf %d %Lf %Lf %d %d %d %s %Lf %Lf %Lf",&mjd,&tudep,&period,&i0,&freq_mi_canal,&DM,&i1,&code_obs,&i2,nom_PSR,&initphase,&tps_integration,&freq);		// Scanne la ligne et range 																																										les éléments du fichier dans les adresses des variables adéquates.
			while(!feof(pf))			// Critère d'arrêt : Si le pointeur du fichier est nul.
 			{
 				// Scan 2 //
 				fgets(valeurs,256,pf);			// Lit la ligne et la copie dans la chaîne : valeurs.
 				sscanf(valeurs,"%d %Lf %Lf %Lf %Lf",&position,&valeur_colonne1,&valeur_colonne2,&valeur_colonne3,&valeur_colonne4);			// Scanne la ligne de valeurs et range les éléments dans les variables adéquates.
 				tab_colonne1[indice]=valeur_colonne1;			// Ajoute au tableau_colonne1 la valeur_colonne1 pour la ligne d'indice : indice.
 				if(max<=valeur_colonne1)		// Si la valeur obtenue dans le scan est plus grande que le maximum.
 				{
 					max=valeur_colonne1;			// La variable max prend la valeur de cet élément.
 					indice_max=indice;			// Sauvegarde de la position du max dans la variable : indice_max.
 				}
 				indice++;			// Incrémente indice.
 			}
 			fclose(pf);			// Ferme le fichier.
 		}
		else			// Si le fichier ne s'ouvre pas.
		{
			cout<<"Impossible d'ouvrir le fichier."<<endl;			// Alerte l'utilisateur : le fichier ne s'est pas ouvert.
		}
		
		if(premier_tour) 	// Si c'est le premier tour de boucle.
		{
			char nom_t2[256];		// Déclare une variable (de type char) pour stocker le nom du fichier '.t2' que l'utilisateur va entrer.
			cout<<"Donnez le nom du fichier '.t2' que vous voulez créer : ";		// Affiche à l'utilisateur ce qu'il doit faire.
			cin>>nom_t2;		// Enregistre le nom qu'entre l'utilisateur au clavier dans la variable nom_t2.
			char *nom=strcat(nom_t2,".t2");			// A ce nom_t2, on rajoute l'extension '.t2'.
			nouveau_fichier.open(nom, ios::out);			// Créé un fichier '.t2' qui porte le nom_t2.
			if (nouveau_fichier.bad()) 			// Si le fichier ne s'est pas créé.
			{
				cout<<"Impossible de créer le fichier."<<endl;			// Alerte l'utilisateur : Le fichier ne s'est pas créé.
			}
			else		// S'il est créé correctement.
			{
				nouveau_fichier<<"FORMAT 1"<<endl;			// Remplit la première ligne du fichier avec "FORMAT 1".
			}
		}
		else 		// Si le fichier est déjà créé.
		{
			char lab;			// Définit une variable lab (de type char) pour identifier l'observatoire d'où proviennent les données.
			if (nouveau_fichier.bad()) 			// Si le fichier ne s'ouvre pas.
			{
				cout<<"Impossible d'ouvrir le fichier."<<endl;			// Alerte l'utilisateur : Le fichier ne peut pas s'ouvrir.
			}
			else		// S'il s'ouvre.
			{
				if(code_obs==15)			// S'il s'agit bien du code de l'observatoire de Nançay.
				{
					lab='f';			// On enregistre la provenance des données comme étant l'observatoire de Nançay.
				}
				else		// S'il ne s'agit pas de l'observatoire de Nançay.
				{
					lab='?';			// On enregistre la provenance des données inconnue.
				}
				nouveau_fichier<<nom_fichier<<" "<<setprecision(7)<<freq_mi_canal<<" ";			// Remplit le fichier du nom du fichier '.asc' puis de la fréquence de mi-canal.
				nouveau_fichier<<setprecision(21)<<ToA(mjd, tudep, period, indice_max, i1, initphase)<<" ";			// Remplit le fichier avec le résultat du calcul du temps d'arrivée. 
				nouveau_fichier<<setprecision(5)<<alea(0.001,99.999)<<" "<<lab<<" "<<"-i BON"<<endl;			// Remplit le fichier de l'incertitude, du nom de l'observatoire et de "-i BON".
			}
		}
		premier_tour=false;		// Les actions à ne faire qu'une seule fois ont été faite, donc on passe premier_tour à false.
	}
	nouveau_fichier.close();			// Ferme le fichier.
	
	/*** Fin du programme ***/
	
	return 0;			// Cloture le main().
}
